#include<iostream>
using namespace std;

#include "RowColumnClass.h"

void RowColumnClass::setRowCol(
     int inRow,   //provided index of row
     int inCol    //provided index of column
     )
{
  indexOfRow= inRow;
  indexOfColumn= inCol;
}

void RowColumnClass::setRow(int inRow)
{
  indexOfRow= inRow;
}

void RowColumnClass::setCol(int inCol)
{
  indexOfColumn= inCol;
}

int RowColumnClass::getRow() const
{
  return indexOfRow;   //we can not modify the indexOfRow, so we can
}                      //use this function to get the index of row

int RowColumnClass::getCol() const
{
  return indexOfColumn;//the same as above
}

void RowColumnClass::addRowColTo(RowColumnClass &inRowCol)
{
  indexOfRow= indexOfRow+ inRowCol.indexOfRow;
  indexOfColumn= indexOfColumn+ inRowCol.indexOfColumn;
}

void RowColumnClass::printRowCol()
{
  cout<<"["<<indexOfRow <<","<<indexOfColumn <<"]";
}


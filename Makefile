yxxu2: ColorClass.o RowColumnClass.o ColorImageClass.o HouseClass.o RoadClass.o CityClass.o printBool.o yxxu2.o
	g++ ColorClass.o RowColumnClass.o ColorImageClass.o HouseClass.o RoadClass.o CityClass.o printBool.o yxxu2.o -o yxxu2
	
imageOpsExec: ColorClass.o RowColumnClass.o ColorImageClass.o printBool.o imageOpsExec.o
	g++ ColorClass.o RowColumnClass.o ColorImageClass.o printBool.o imageOpsExec.o -o imageOpsExec
	
imageOpsExec.o: imageOpsExec.cpp
	g++ -c imageOpsExec.cpp -o imageOpsExec.o
	
yxxu2.o: yxxu2.cpp
	g++ -c yxxu2.cpp -o yxxu2.o
	
printBool.o: printBool.cpp printBool.h
	g++ -c printBool.cpp -o printBool.o

ColorImageClass.o: ColorImageClass.cpp ColorImageClass.h
	g++ -c ColorImageClass.cpp -o ColorImageClass.o

RowColumnClass.o: RowColumnClass.cpp RowColumnClass.h
	g++ -c RowColumnClass.cpp -o RowColumnClass.o

ColorClass.o: ColorClass.cpp ColorClass.h
	g++ -c ColorClass.cpp -o ColorClass.o
	
HouseClass.o: HouseClass.cpp HouseClass.h
	g++ -c HouseClass.cpp -o HouseClass.o
	
RoadClass.o: RoadClass.cpp RoadClass.h
	g++ -c RoadClass.cpp -o RoadClass.o

CityClass.o: CityClass.cpp CityClass.h
	g++ -c CityClass.cpp -o CityClass.o

clean:
	rm -rf ColorClass.o RowColumnClass.o ColorImageClass.o HouseClass.o RoadClass.o CityClass.o printBool.o yxxu2 imageOpsExec

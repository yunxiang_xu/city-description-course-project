#include<iostream>
using namespace std;

#include "ColorClass.h"


void ColorClass::setToBlack()     //set the color to black
{
  valueOfColor[0]= 0;
  valueOfColor[1]= 0;
  valueOfColor[2]= 0;
}

void ColorClass::setToRed()      //set the color to red
{
  valueOfColor[0]= MAX_COLOR_VAL;
  valueOfColor[1]= 0;
  valueOfColor[2]= 0;
}

void ColorClass::setToGreen()     //set the color to green
{
  valueOfColor[0]= 0;
  valueOfColor[1]= MAX_COLOR_VAL;
  valueOfColor[2]= 0;
}

void ColorClass::setToBlue()     //set the color to blue
{
  valueOfColor[0]= 0;
  valueOfColor[1]= 0;
  valueOfColor[2]= MAX_COLOR_VAL;
}

void ColorClass::setToWhite()    //set the color to white
{
  valueOfColor[0]= MAX_COLOR_VAL;
  valueOfColor[1]= MAX_COLOR_VAL;
  valueOfColor[2]= MAX_COLOR_VAL;
}

bool ColorClass::setTo(
     int inRed,
     int inGreen,
     int inBlue
     )               //set the color to the provided value
{
  valueOfColor[0]= inRed;
  valueOfColor[1]= inGreen;
  valueOfColor[2]= inBlue;
  if (inRed<= MAX_COLOR_VAL&& inRed>=0
      &&inGreen<= MAX_COLOR_VAL&& inGreen>=0
      &&inBlue<=MAX_COLOR_VAL&& inBlue>=0)   //judge if need clipping
  {
    return false;
  }
  else
  {
    for (int i= 0; i< 3; i++)      //if out of the range,
    {                              //do the clipping
      if (valueOfColor[i]> MAX_COLOR_VAL)
      {
        valueOfColor[i]= MAX_COLOR_VAL;
      }
      else
      {
        if (valueOfColor[i]< 0)
        {
          valueOfColor[i]= 0;
        }
      }
    }
    return true;
  }
}

bool ColorClass::setTo(ColorClass &inColor)    //set the color to the provided color
{
  int inRed= inColor.valueOfColor[0];
  int inGreen= inColor.valueOfColor[1];
  int inBlue= inColor.valueOfColor[2];
  bool state= setTo(inRed, inGreen, inBlue); //use the setTo function 
  if (state== true)
  {
    return true;
  }
  else
  {
    return false;
  }
}

bool ColorClass::addColor(ColorClass &rhs)
{
  int inRed= valueOfColor[0]+ rhs.valueOfColor[0];  //set the value of
  int inGreen= valueOfColor[1]+ rhs.valueOfColor[1];//the three components
  int inBlue= valueOfColor[2]+ rhs.valueOfColor[2]; //and use the former
  bool state= setTo(inRed, inGreen, inBlue);        //setTo function
  if (state== true)
  {
    return true;
  }
  else
  {
    return false;
  }
}

bool ColorClass::subtractColor(ColorClass &rhs)
{
  int inRed= valueOfColor[0]- rhs.valueOfColor[0];  //set the value of
  int inGreen= valueOfColor[1]- rhs.valueOfColor[1];//the three components
  int inBlue= valueOfColor[2]- rhs.valueOfColor[2]; //and use the former
  bool state= setTo(inRed, inGreen, inBlue);        //setTo function
  if (state== true)
  {
    return true;
  }
  else
  {
    return false;
  }
}

bool ColorClass::adjustBrightness(double adjFactor)
{
  int inRed= (int)valueOfColor[0]* adjFactor;   //set the value of the
  int inGreen= (int)valueOfColor[1]* adjFactor; //three components of 
  int inBlue= (int)valueOfColor[2]* adjFactor;  //the color and use the
  bool state= setTo(inRed, inGreen, inBlue);    //former setTo function
  if (state== true)
  {
    return true;
  }
  else
  {
    return false;
  }
}

void ColorClass::printComponentValues()
{
  cout<<"R: "<<valueOfColor[0];
  cout<<" G: "<<valueOfColor[1];
  cout<<" B: "<<valueOfColor[2];
}

int ColorClass::getRedValue() const
{
  return valueOfColor[0];
}

int ColorClass::getGreenValue() const
{
  return valueOfColor[1];
}

int ColorClass::getBlueValue() const
{
  return valueOfColor[2];
}


#ifndef _COLORCLASS_H
#define _COLORCLASS_H

class ColorClass //to describe a color
{
private:
  const static int MAX_COLOR_VAL= 255;
  int valueOfColor[3];  //indicate the values of red, green and blue
  
public:
  void setToBlack();    //set the color to black
  void setToRed();      //set the color to red
  void setToGreen();    //set the color to green
  void setToBlue();     //set the color to blue
  void setToWhite();    //set the color to white
  bool setTo(
       int inRed,
       int inGreen,
       int inBlue
       );                             //set the color to the provided value
  bool setTo(ColorClass &inColor);    //set the color to the provided color
  bool addColor(ColorClass &rhs);     //add values of the color
  bool subtractColor(ColorClass &rhs);//subtract values of the color
  bool adjustBrightness(double adjFactor);//ajust brightness of the color
  void printComponentValues();        //print the values of the color
  int getRedValue() const;    //get the value of red
  int getGreenValue() const;  //get the value of green
  int getBlueValue() const;   //get the value of blue
};

#endif

#include<iostream>
#include<string>
using namespace std;

#include "ColorImageClass.h"
#include "CityClass.h"
#include "printBool.h"

//Programmer: Yunxiang Xu
//Date: February 2015
//Purpose: The purpose of this program is to read, print and draw a city

const int MENU_LOAD= 1;           //the menu choice setup
const int MENU_PRINT= 2;
const int MENU_DRAW= 3;
const int MENU_QUIT= 4;

int main()
{
  int menuChoice;                 //the choice by the user
  CityClass curCity;              //the current city
  string iFname;                  //name of the file to read
  string oFname;                  //name of the file to print
  string mapName;                 //name of the file to draw map on
  bool statePrint;                //indicate if the opreation is success
  bool stateLooping= true;        //indicate if the program is contiuning
  while (stateLooping)
  {
    cout<< "1: Read a city description file"<< endl;
    cout<< "2: Print out a city description"<< endl;
    cout<< "3: Draw city map to current image (clears out current image)"<< endl;
    cout<< "4: Quit the program"<< endl;
    cout<< "Your choice:";
    cin>> menuChoice;                   //let the user to make the choice
    if (cin.fail())
    {
      cin.clear();
      cin.ignore(200, '\n');
      cout<< "Your Choice is invalid! Try again!"<< endl;  
    }                                   //if the input is not an int
    else
    {
      switch (menuChoice)
      {
      case MENU_LOAD:                            //To load a file,
        cout<< "Input the name of the file:";    //If the file does not exist
        cin>> iFname;                            //or damaged or format is not
        statePrint= curCity.readFromFile(iFname);//right, the operation will
        printBool("Load success", statePrint);   //fail and get an error.
        break;

      case MENU_PRINT:
        if (!curCity.getIsReadyForUse())         //If nothing has been loaded
        {
          cout<< "ERROR: No city loaded successfully yet!"<< endl;
          statePrint= false;
        }
        else
        {
          statePrint= statePrint= curCity.printCity();
        }
        printBool("Print success", statePrint);
        break;

      case MENU_DRAW:
        if (!curCity.getIsReadyForUse())         //If notthing has been loaded
        {
          cout<< "ERROR: No city loaded successfully yet!"<< endl;
          statePrint= false;
        }
        else
        {
          cout<< "Write the name of the map:";   //input the file name
          cin>> mapName;                         //and draw the map
          statePrint= curCity.drawMap(mapName);
        }                                        //so save the map
        printBool("Draw success", statePrint);
        break;

      case MENU_QUIT:
        stateLooping= false;                     //Stop the looping
        cout<< "Thank you for using this program!"<< endl;
        break;

      default:
        cout<< "Your Choice is invalid! Try again!"<< endl;
        break;                                   //if the choice is not valid
      }
    }
  }
}

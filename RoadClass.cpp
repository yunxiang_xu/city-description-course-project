#include<iostream>
#include<fstream>
#include<cstdlib>
#include<string>
using namespace std;

#include "RoadClass.h"

void RoadClass::setNumLanes(int inNum)
{
  numberOfLanes= inNum;
}

bool RoadClass::setDirection(const string &inDirec)
{
  if (inDirec== "ns"|| inDirec== "we")
  {                                     //if the direction is not ns or we
    direction= inDirec;                 //do not set
    return true;
  }
  else
  {
    return false;
  }
}

void RoadClass::setLoc(double inXLoc, double inYLoc)
{
  xLocation= inXLoc;                    //set the coords of the road
  yLocation= inYLoc;
}

void RoadClass::setLength(double inLen)
{
  roadLength= inLen;                    //set the length of the road
}

int RoadClass::getNumLanes() const
{
  return numberOfLanes;                 //getter function to get the
}                                       //number of lanes, the direction,
                                        //the x and y coords and the length
string RoadClass::getDirection() const
{
  return direction;
}

double RoadClass::getXLocation() const
{
  return xLocation;
}

double RoadClass::getYLocation() const
{
  return yLocation;
}

double RoadClass::getLength() const
{
  return roadLength;
}

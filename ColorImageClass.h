#ifndef _COLORIMAGECLASS_H
#define _COLORIMAGECLASS_H

#include "ColorClass.h"
#include "RowColumnClass.h"

class ColorImageClass //to represent a small image
{
private:
  string magicNum;
  const static int MAX_VAL= 255;
  const static int MAX_NUM_OF_ROWS= 300;         //indicate the fixed number
  const static int MAX_NUM_OF_COLS= 350;         //of the rows and columns
  ColorClass imageColor[MAX_NUM_OF_COLS][MAX_NUM_OF_ROWS]; //of the image
  int numRows;
  int numCols;
  bool fileState;                             //indicate the state of file

public:
  ColorImageClass();
  void initializeTo(ColorClass &inColor);     //initialize the color of every
  bool adjustBrightness(double adjFactor);    //adjust the brightness of all
  bool addImageTo(ColorImageClass &rhsImg);    //add one image to the current
  bool addImages(
       int numImgsToAdd,                   //number of the images
       ColorImageClass imagesToAdd[]       //the images to add
       );
  bool setColorAtLocation(
       RowColumnClass &inRowCol,     //the position of the pixel
       ColorClass &inColor           //the color proviede to set the pixel
       );
  bool getColorAtLocation(
       RowColumnClass &inRowCol,     //the position of the pixel
       ColorClass &outColor          //the color of the pixel
       );
  bool readFromFile(const string &inFname); //to read one image from file
  bool writeToFile(const string &outFname); //to output the image to a file
  void setSizeMax();                        //to set the size of the image
  int getNumRows() const;                   //to get the num of rows
  int getNumCols() const;                   //to get the num of cols
  bool checkRowColValid(const RowColumnClass &inRowCol);
  //to check if the location is valid
  bool getIsReadyForUse() const;
  //to get the state of weather the image is ready to use
};

#endif

#include <iostream>
#include <cstdlib> //for system
using namespace std;

#include "ColorClass.h"
#include "RowColumnClass.h"
#include "ColorImageClass.h"
#include "printBool.h"

//Programmer: Andrew M. Morgan
//Purpose: A menu-driven program to allow a user to load a PPM
//         image, perform some image operations, and write
//         PPM images out to file.  Developed for EECS402.
//Date: February 2015

//Menu option constants...
const int MENU_LOAD = 1;
const int MENU_ADJUST_BRIGHTNESS = 2;
const int MENU_ADD_IMAGE = 3;
const int MENU_SAVE = 4;
const int MENU_QUIT = 5;
const int MAX_MENU_CHOICE = MENU_QUIT;

int main()
{
  double adjFactor;
  bool didClip;
  string imgFname;
  bool keepLooping;
  bool success;
  ColorImageClass curImage;
  int userChoice;

  //Loop until user chooses to quit the program
  keepLooping = true;

  while (keepLooping)
  {
    cout << "1. Load an image from a PPM file" << endl;
    cout << "2. Adjust the brightness of the current image" << endl;
    cout << "3. Add a (same-size) image to current image" << endl;
    cout << "4. Write the current image to a PPM file" << endl;
    cout << "5. Quit the program" << endl;
    cout << "Your choice: ";
    cin >> userChoice;

    if (!cin || userChoice < 1 || userChoice > MAX_MENU_CHOICE)
    {
      //If user provided an invalid value, then clear out the
      //input buffer and re-prompt
      cin.clear();
      cin.ignore(200, '\n');

      cout << "Invalid entry - try again!" << endl;
    }
    else
    {
      if (userChoice == MENU_LOAD)
      {
        cout << "Enter input image name: ";
        cin >> imgFname;
        success = curImage.readFromFile(imgFname);

        printBool("Load success", success);
      }
      else if (userChoice == MENU_ADJUST_BRIGHTNESS)
      {
        if (!curImage.getIsReadyForUse())
        {
          cout << "ERROR: No image loaded successfully yet!" << endl;
          success = false;
        }
        else
        {
          cout << "Enter adjustment factor: ";
          cin >> adjFactor;
          if (!cin || adjFactor <= 0)
          {
            cout << "Adjustment factor must be greater than 0!" << endl;
            cout << "   Aborting operation!" << endl;
          }
          else
          {
            didClip = curImage.adjustBrightness(adjFactor);

            printBool("Clipped: ", didClip);
          }
        }
      }
      else if (userChoice == MENU_ADD_IMAGE)
      {
        if (!curImage.getIsReadyForUse())
        {
          cout << "ERROR: No image loaded successfully yet!" << endl;
          success = false;
        }
        else
        {
          ColorImageClass imgToAdd;

          cout << "Enter input image name to add: ";
          cin >> imgFname;
          success = imgToAdd.readFromFile(imgFname);
          printBool("Load success", success);

          if (!(curImage.getNumRows() == imgToAdd.getNumRows() &&
                curImage.getNumCols() == imgToAdd.getNumCols()))
          {
            cout << "ERROR: Images must be the same size to add!" << endl;
            success = false;
          }
          else
          {
            curImage.addImageTo(imgToAdd);
          }

          printBool("Add image success: ", success); 
        }
      }
      else if (userChoice == MENU_SAVE)
      {
        if (!curImage.getIsReadyForUse())
        {
          cout << "ERROR: No image loaded successfully yet!" << endl;
          success = false;
        }
        else
        {
          cout << "Enter output image name: ";
          cin >> imgFname;
          success = curImage.writeToFile(imgFname);

          printBool("Write success", success);
        }
      }
      else if (userChoice == MENU_QUIT)
      {
        keepLooping = false;
      }
    }
  } //end while keepLooping

  cout << "Thanks for using this program!" << endl;
  
#if 0
  //------Linux-based way to easily view outputs ------
  ofstream htmlFile;
  htmlFile.open("index.html"); //hacking a test here - no error checking
  htmlFile << "<HTML><BODY>" << endl;
  htmlFile << "  <TABLE>" << endl;

  htmlFile << "    <TR><TD>Input Img</TD>" << 
              "<TD><IMG SRC='bigHouse.png'></TD></TR>" << endl;
  system("convert bigHouse.ppm bigHouse.png");

  htmlFile << "    <TR><TD>Straight Out</TD>" << 
              "<TD><IMG SRC='bigHouseOut.png'></TD></TR>" << endl;
  system("convert bigHouseOut.ppm bigHouseOut.png");
  
  htmlFile << "    <TR><TD>Brighter 200</TD>" << 
              "<TD><IMG SRC='bigHouseBrighter200.png'></TD></TR>" << endl;
  system("convert bigHouseBrighter200.ppm bigHouseBrighter200.png");
  
  htmlFile << "  </TABLE>" << endl;
  htmlFile << "</BODY></HTML>" << endl;
  htmlFile.close();
#endif
  return 0;
}

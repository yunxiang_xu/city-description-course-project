Script started on Fri 13 Feb 2015 07:10:08 PM EST
[ 201 ] run -: ../morgana2
1: Read a city description file
2: Print out a city description
3: Draw city map to current image (clears out current image)
4: Quit the program
Your choice: 1
Enter input city description filename: cityA.txt
Load success: TRUE
1: Read a city description file
2: Print out a city description
3: Draw city map to current image (clears out current image)
4: Quit the program
Your choice: 2
City Contents:
  Houses: 
    Value: $57000 XY: 495 795 RGB: R: 255 G: 0 B: 0
    Value: $157000 XY: 1650 1650 RGB: R: 255 G: 255 B: 0
    Value: $250000 XY: 2000 907.5 RGB: R: 127 G: 255 B: 127
    Value: $450500 XY: 2070 1600 RGB: R: 255 G: 0 B: 255
  Roads: 
    Lanes: 2 Dir: we XY: 82.5 825 Length: 2723
    Lanes: 6 Dir: ns XY: 1485 825 Length: 1606.25
    Lanes: 4 Dir: we XY: 1485 2332.25 Length: 1237.5
    Lanes: 4 Dir: ns XY: 2100 1771.25 Length: 660
1: Read a city description file
2: Print out a city description
3: Draw city map to current image (clears out current image)
4: Quit the program
Your choice: 3
Enter output image name: mapA.ppm
Write success: TRUE
1: Read a city description file
2: Print out a city description
3: Draw city map to current image (clears out current image)
4: Quit the program
Your choice: 4
Thanks for using this program!
[ 202 ] run -: exit
exit

Script done on Fri 13 Feb 2015 07:10:21 PM EST

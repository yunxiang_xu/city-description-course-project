#ifndef _ROADCLASS_H
#define _ROADCLASS_H

#include "RowColumnClass.h"

class RoadClass
{
private:
  int numberOfLanes;                    //the number of the lanes
  string direction;                     //the direction of the road
  double roadLength;                    //the length of the road
  double xLocation;                     //the x coord
  double yLocation;                     //the y coord
public:
  void setNumLanes(int inNum);          //set the num of the lanes
  bool setDirection(const string &inDirec);//set the direction of the road
  void setLoc(                          //set the xy coords of the road
       double inXLoc,
       double inYLoc
       );
  void setLength(double inLen);         //set the length of the road
  int getNumLanes() const;              //get the num of lanes
  string getDirection() const;          //get the direction
  double getXLocation() const;          //get the x coord
  double getYLocation() const;          //get the y coord
  double getLength() const;             //get the length
};

#endif

#ifndef _HOUSECLASS_H
#define _HOUSECLASS_H

#include "ColorClass.h"
#include "RowColumnClass.h"

class HouseClass
{
private:
  int value;             //value of the house
  double xLocation;      //xLocation of the house
  double yLocation;      //yLocation of the house
  ColorClass houseColor; //color of the house
  const static int valueDim= 15000; //price for one unit
public:
  void setValue(int inVal);
  //To set the value of the house from the provided value
  void setLoc(
       double inXLoc,    //provided xLocation
       double inYLoc     //provided yLocation
       );
  //To set the location of the house
  bool setColor(
       int inRed,
       int inGreen,
       int inBlue
       );
  //To set the color of the house
  int getValue() const;         //return the value of the house
  double getXLocation() const;  //return the xLocation of the house
  double getYLocation() const;  //return the yLocation of the house
  int computeDim() ;            //compute the dimension of the house
  ColorClass getColor() const;  //get the color of the house
};
#endif

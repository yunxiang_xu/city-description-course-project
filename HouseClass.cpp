#include<iostream>
#include<fstream>
#include<cstdlib>
#include<string>
using namespace std;

#include "HouseClass.h"


void HouseClass::setValue(int inVal)
{                                               //set the value of the house
  value= inVal;
}

void HouseClass::setLoc(double inXLoc, double inYLoc)
{                                               //set the xy location of the
  xLocation= inXLoc;                            //house
  yLocation= inYLoc;
}

bool HouseClass::setColor(int inRed, int inGreen, int inBlue)
{                                               //set the color of the house
  return houseColor.setTo(inRed, inGreen, inBlue);
}

int HouseClass::getValue() const
{                                               //to get the value
  return value;
}

double HouseClass::getXLocation() const
{                                               //to get the x location
  return xLocation;
}

double HouseClass::getYLocation() const
{                                               //to get the y location
  return yLocation;
}

int HouseClass::computeDim()
{                                               //to compute the dimension
  return int(value/valueDim+1);
}

ColorClass HouseClass::getColor() const
{                                               //to get the color
  return houseColor;
}


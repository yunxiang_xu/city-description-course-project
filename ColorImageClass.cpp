#include<iostream>
#include<fstream>
#include<cstdlib>
#include<string>
using namespace std;

#include "ColorImageClass.h"

ColorImageClass::ColorImageClass()  //ctor
{                                   //set the file state as false
  fileState= false;
}


void ColorImageClass::initializeTo(ColorClass &inColor)
{                                          //every pixel  
  for (int i= 0; i< MAX_NUM_OF_ROWS; i++)
  {
    for (int j= 0; j< MAX_NUM_OF_COLS; j++)
    {
      imageColor[j][i].setTo(inColor);
    }
  }
}

bool ColorImageClass::adjustBrightness(double adjFactor)
{                                          //the pixels in the image
  int numOfCliping= 0;
  bool stateClip;
  for (int i= 0; i< MAX_NUM_OF_ROWS; i++)
  {
    for (int j= 0; j< MAX_NUM_OF_COLS; j++)
    {
      stateClip= imageColor[j][i].adjustBrightness(adjFactor);
      if (stateClip== true)
      {
        numOfCliping++;                   //every time the addColor funtion
      }
    }
  }
  if (numOfCliping> 0)
  {
    return true;
  }
  else
  {
    return false;
  }
}
     

bool ColorImageClass::addImageTo(ColorImageClass &rhsImg)
{
  int numOfCliping= 0;                    //calculate the times of clipping
  for (int i= 0; i< MAX_NUM_OF_ROWS; i++)
  {
    for (int j= 0; j< MAX_NUM_OF_COLS; j++)
    {
      bool state= imageColor[j][i].addColor(rhsImg.imageColor[j][i]);
      if (state== true)
      {
        numOfCliping++;                   //every time the addColor funtion
      }                                   //return true, times of clipping
    }                                     //plus one
  }
  if (numOfCliping> 0)
  {
    return true;
  }
  else
  {
    return false;
  }
}

bool ColorImageClass::addImages(
     int numImgsToAdd,                   //number of the images
     ColorImageClass imagesToAdd[]       //the images to add
     )
{
  ColorImageClass sumOfImgages;          //set one image as the sum
  int timesOfCliping= 0;                 //the times of clipping
  ColorClass blackColor;                 //set the black color
  blackColor.setToBlack();
  sumOfImgages.initializeTo(blackColor); //initialize the sum to black
  for (int i= 0; i<numImgsToAdd; i++)
  {
    bool state= sumOfImgages.addImageTo(imagesToAdd[i]);
    if (state== true)
    {
      timesOfCliping++;
    }
  }
  for (int i= 0; i< MAX_NUM_OF_ROWS; i++)    //set the image to the sum
  {
    for (int j= 0; j< MAX_NUM_OF_COLS; j++)
    {
      imageColor[j][i].setTo(sumOfImgages.imageColor[j][i]);
    }
  }
  if (timesOfCliping> 0)
  {
    return true;
  }
  else
  {
    return false;
  }
}

bool ColorImageClass::setColorAtLocation(
     RowColumnClass &inRowCol,     //the position of the pixel
     ColorClass &inColor           //the color proviede to set the pixel
     )
{
  if (inRowCol.getRow()< MAX_NUM_OF_ROWS&& inRowCol.getRow()>= 0&&
      inRowCol.getCol()< MAX_NUM_OF_COLS&& inRowCol.getCol()>=0)
      //to judge if the location is valid
  {
    imageColor[inRowCol.getCol()][inRowCol.getRow()].setTo(inColor);
    return true;
   }
   else
   {
    return false;
   }
}

bool ColorImageClass::getColorAtLocation(
     RowColumnClass &inRowCol,     //the position of the pixel
     ColorClass &outColor          //the color of the pixel
     )
{
  if (inRowCol.getRow()< MAX_NUM_OF_ROWS&& inRowCol.getRow()>= 0&&
      inRowCol.getCol()< MAX_NUM_OF_COLS&& inRowCol.getCol()>=0)
      //to judge if the location is valid
  {
    outColor.setTo(imageColor[inRowCol.getCol()][inRowCol.getRow()]);
    return true;
  }
  else 
  {
    return false;
  }
}

bool ColorImageClass::getIsReadyForUse() const
{
  return fileState;
}

bool ColorImageClass::readFromFile(const string &inFname)
{
  int inRed;
  int inGreen;
  int inBlue;
  int maxVal;
  ifstream inFile;
  inFile.open((inFname+ ".ppm").c_str());
  if (inFile.fail())                    //if fail to open the file
  {
    cout<< "ERROR: Unable to open file"<< endl;
    inFile.clear();
    inFile.ignore(200, '\n');
    fileState= false;
  }
  else
  {
    inFile>> magicNum;
    if (magicNum!="P3")                 //if the magic number is not P3,
    {                                   //then it is not a valid ppm file
      fileState= false;
      cout<< "ERROR: The format is wrong!"<< endl;
    }
    else
    {
      inFile>> numCols;
      inFile>> numRows;
      inFile>> maxVal;
      if (maxVal!= MAX_VAL||                        //If the max value of RGB
          numCols< 0|| numCols> MAX_NUM_OF_COLS||   //is not same as it should
          numRows< 0|| numRows> MAX_NUM_OF_ROWS)    //or the num of cols and
      {                                             //rows are out the image
        cout<< "ERROR: This file is not standard!"<< endl;
        fileState= false;
        inFile.close();
      }
      else
      {
        for (int i= 0; i< numRows; i++)
        {
          for (int j= 0; j< numCols; j++)
          {
            inFile>> inRed;
            inFile>> inGreen;
            inFile>> inBlue;
            imageColor[j][i].setTo(inRed, inGreen, inBlue);
          }
        }
        if (inFile.eof()||inFile.fail())        //if the data is missing or 
        {                                       //some data is wrong, program
          inFile.clear();                       //consider the image damaged
          inFile.ignore(200, '\n');
          cout<< "ERROR: The file is damaged"<< endl;
          fileState= false;
        }
        else
        {
          inFile.close();
          fileState= true;
        }
      }
    }
  }
  return fileState;
}

bool ColorImageClass::writeToFile(const string &outFname)
{
  ofstream outFile;                             //if the file is  unable to 
  outFile.open((outFname+".ppm").c_str());      //open
  if (outFile.fail())
  {
    cout<< "ERROR: Unable to open output file!"<< endl;
    outFile.clear();
    return false;
  }
  else
  {
    outFile<< "P3 "<< endl;                             
    outFile<< numCols<< " "<< numRows<< " "<< endl;
    outFile<< MAX_VAL<< " "<< endl;             //write out the specs
    for (int i= 0; i< numRows; i++)
    {
      for (int j= 0; j< numCols; j++)
      {
        outFile<< imageColor[j][i].getRedValue()<< " ";
        outFile<< imageColor[j][i].getGreenValue()<< " ";
        outFile<< imageColor[j][i].getBlueValue()<< " ";
      }
      outFile<< endl;
    }
    outFile.close();
    return true;
  }
}

int ColorImageClass::getNumRows() const
{
  return numRows;
}

int ColorImageClass::getNumCols() const
{
  return numCols;
}

bool ColorImageClass::checkRowColValid(const RowColumnClass &inRowCol)
{
  if (inRowCol.getRow()>= 0&& inRowCol.getRow()< numRows&&
      inRowCol.getCol()>= 0&& inRowCol.getCol()< numCols)
  {
    return true;                        //return true if the inRowCol is
  }                                     //inside the image
  else
  {
    return false;
  }
}

void ColorImageClass::setSizeMax()
{
  numRows= MAX_NUM_OF_ROWS;
  numCols= MAX_NUM_OF_COLS;
}



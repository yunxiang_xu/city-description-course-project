#ifndef _ROWCOLUMNCLASS_H
#define _ROWCOLUMNCLASS_H

class RowColumnClass  //to uniquely identify a specific pixel within an image
{
  private:

    int indexOfRow;      //indicate the index of row
    int indexOfColumn;   //indicate the index of column

  public:

    void setRowCol(
         int inRow,   //provided index of row
         int inCol    //provided index of column
         );
    void setRow(int inRow);
    void setCol(int inCol);
    int getRow() const;
    int getCol() const;
    void addRowColTo(RowColumnClass &inRowCol);
    void printRowCol();
    
};

#endif

#include <iostream>
#include <string>
using namespace std;

#include "printBool.h"

//Programmer: Andrew Morgan
//Date: Feb 2015
//Purpose: Implement an easy-to-use function to consistently print
//         a boolean variable
void printBool(const string &label, const bool inBool)
{
  cout << label << ": ";
  if (inBool)
  {
    cout << "TRUE" << endl;
  }
  else
  {
    cout << "FALSE" << endl;
  }
}


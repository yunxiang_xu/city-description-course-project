#ifndef _PRINTBOOL_H_
#define _PRINTBOOL_H_

#include <string>
using namespace std;

//Programmer: Andrew Morgan
//Date: Feb 2015
//Purpose: Provide a function to print the value of a boolean
//         with a specified "prefix"..  For EECS402
void printBool(const string &label, const bool inBool);

#endif // _PRINTBOOL_H_

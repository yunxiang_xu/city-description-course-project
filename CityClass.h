#ifndef _CITYCLASS_H
#define _CITYCLASS_H

#include "HouseClass.h"
#include "RoadClass.h"
#include "ColorClass.h"
#include "ColorImageClass.h"


class CityClass
{
private:
  const static int MAX_NUM_HOUSES= 15;
  //The max number of the houses as stated in the requirement, a city contains
  //at most 15 houses.
  const static int MAX_NUM_ROADS= 15;
  //The max number of the roads as stated in the requirment, a city contains
  //at most 15 roads.
  HouseClass house[MAX_NUM_HOUSES];
  //The house array containing all the houses;
  RoadClass road[MAX_NUM_ROADS];
  //The road array contains all the house;
  int numHouses;
  //The number of the houses
  int numRoads;
  //the number of the roads
  bool fileState;
  //to indicate whether the file is ready to use

public:
  CityClass();
  int computeLocMap(double locationDis);
  //To transfer the location distance to the number of pixels in the image
  bool readFromFile(const string &inFname);
  //Read the city specs from a txt file
  bool printCity();
  //print the description of the city
  bool drawMap(const string &outFname);
  //draw a map of the city
  bool getIsReadyForUse() const;
  //to get the state of whether the file is ready for use
};
#endif

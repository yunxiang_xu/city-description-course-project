#include<iostream>
#include<fstream>
#include<cstdlib>
#include<string>
using namespace std;

#include "CityClass.h"

CityClass::CityClass()    //ctor
{
  fileState= false;       //to set the fileState as false
}


int CityClass::computeLocMap(double locationDis)
{
  return int(locationDis/ 8.25+ 0.5);    //return int value for the image
}

bool CityClass::readFromFile(const string &inFname)
{
  int value;
  double xLocation;
  double yLocation;
  int valRed;
  int valGreen;
  int valBlue;
  int numLanes;
  double length;
  string roadDirection;                 //"we" or "ns"
  string memberTitle;                   //"road" or "house"
  numHouses= 0;                         //initialize the num of houses
  numRoads= 0;                          //and roads as zero
  ifstream inFile;
  inFile.open((inFname+ ".txt").c_str());
  if (inFile.fail())                    //check if the file is valid
  {
    inFile.clear();
    inFile.ignore(200, '\n');
    cout<< "ERROR: Unable to open file"<< endl;
    fileState= false;
  }
  else
  {
    bool validInputFound= false;        //to determine whether or not to
    while (!validInputFound)            //contiune reading file
    {
      inFile>> memberTitle;
      if (inFile.eof())                 //if getting the end of the file
      {                                 //after reading some items, the
        validInputFound= true;          //reading process should end and
        inFile.clear();                 //return true.
        memberTitle= "end";             //mark: if the txt file is empty,
        fileState= true;                //it is considered as an empty city.
      }
      else 
      {
        if (memberTitle== "house"&& numHouses< MAX_NUM_HOUSES)
        {                               //if the title is house and there are
          inFile>> value;               //less than 15 houses, add a house.
          inFile>> xLocation;
          inFile>> yLocation;
          inFile>> valRed;
          inFile>> valGreen;
          inFile>> valBlue;
          house[numHouses].setValue(value);
          house[numHouses].setLoc(xLocation, yLocation);
          house[numHouses].setColor(valRed, valGreen, valBlue);
          numHouses++;
        }
        else if (memberTitle== "road"&& numRoads< MAX_NUM_ROADS)
        {                               //if the title is road and there are
          inFile>> numLanes;            //less than 15 roads, add a road.
          inFile>> roadDirection;
          inFile>> xLocation;
          inFile>> yLocation;
          inFile>> length;
          road[numRoads].setNumLanes(numLanes);
          road[numRoads].setDirection(roadDirection);
          road[numRoads].setLoc(xLocation, yLocation);
          road[numRoads].setLength(length);
          numRoads++;
        }
        else
        {
          validInputFound= true;
          if(numHouses>= MAX_NUM_HOUSES)//if there are already 15 houses and
          {                             //the program find another house
            cout<< "There are too many house in the city!"<< endl;
          }
          if (numRoads>= MAX_NUM_ROADS) //if there are already 15 roads and
          {                             //the program find another house
            cout<< "There are too many roads in the city!"<< endl;
          }
          cout<< "ERROR: This file does not meet the standard!"<< endl;
          fileState= false;
        }
        if (inFile.fail()|| inFile.eof())
        {                               //if something wrong with the data
          validInputFound= true;        //or misses some data, the program
          inFile.clear();               //considered the city file has been
          inFile.ignore(200, '\n');     //damaged
          cout<< "ERROR: This file is damaged"<< endl;
          fileState= false;
        }
      }
    }
  }
  return fileState;
}


bool CityClass::printCity()
{
  if (!fileState)                       //if nothing has been read,
  {                                     //return false.
    return false;
  }
  else
  {
    cout<< "House List: "<< endl;
    for (int i= 0; i< numHouses; i++)
    {
      cout<< " Value: $"<< house[i].getValue()<< " ";
      cout<< "Coordinate: "<< house[i].getXLocation()<< " ";
      cout<< house[i].getYLocation()<< " ";
      cout<< "RGB: R: "<< house[i].getColor().getRedValue()<< " ";
      cout<< "G: "<< house[i].getColor().getGreenValue()<< " ";
      cout<< "B: "<< house[i].getColor().getBlueValue()<< endl;
    }
    cout<< "Road List:"<< endl;
    for (int i= 0; i< numRoads; i++)
    {
      cout<< " Lanes: "<< road[i].getNumLanes()<< " ";
      cout<< "Direction: "<< road[i].getDirection()<< " ";
      cout<< "Coordinate: "<< road[i].getXLocation()<< " ";
      cout<< road[i].getYLocation()<< " ";
      cout<< "Length: "<< road[i].getLength()<< endl;
    }
    return true;                        //the program automatically print
  }                                     //the houses first and then print
}                                       //print the roads

bool CityClass::drawMap(const string &outFname)
{
  ColorImageClass imageMap;
  RowColumnClass inRowCol;
  ColorClass inColor;
  const int valGreyColor= 127;          //set the value of the RGB of the road
  int numError= 0;                      //to check if any pixel is out of the
  inColor.setToBlack();                 //image
  imageMap.setSizeMax();                //initialize the image to all black
  imageMap.initializeTo(inColor);
  for (int i= 0; i< numHouses; i++)
  {
    inColor= house[i].getColor();
    int xPix= computeLocMap(house[i].getXLocation());
    int yPix= computeLocMap(house[i].getYLocation());
    for (int j= 0; j< house[i].computeDim(); j++)     
    {
      for (int k= 0; k< house[i].computeDim(); k++)
      {
        inRowCol.setCol(xPix+ j);
        inRowCol.setRow(yPix+ k);
        if (!imageMap.checkRowColValid(inRowCol))
        {
          numError++;                   //if the location of the pixel is
        }                               //out of the image, do not do the
        else                            //opearation
        {
          imageMap.setColorAtLocation(inRowCol, inColor);
        }
      }
    }
  }
  inColor.setTo(valGreyColor, valGreyColor, valGreyColor);
  for (int i= 0; i< numRoads; i++)
  {
    for (int j= 0; j< 3*road[i].getNumLanes(); j++)
    {
      for (int k= 0; k< computeLocMap(road[i].getLength()); k++)
      {
        if (road[i].getDirection()== "ns")
        {                               //if the road is ns, go down
          inRowCol.setCol(computeLocMap(road[i].getXLocation())+ j);
          inRowCol.setRow(computeLocMap(road[i].getYLocation())+ k);
        }
        else if (road[i].getDirection()== "we")
        {                               //if the road is we, go right
          inRowCol.setCol(computeLocMap(road[i].getXLocation())+ k);
          inRowCol.setRow(computeLocMap(road[i].getYLocation())+ j);
        }
        if (!imageMap.checkRowColValid(inRowCol))
        {                               //if the pixel is out of the
          numError++;                   //the image.
        }
        else
        {
          imageMap.setColorAtLocation(inRowCol, inColor);
        }
      }
    }
  }
  if (numError== 0)                     //if there are any pixels out
  {                                     //of the image, do not draw
    return imageMap.writeToFile(outFname);
  }
  else
  {
    cout<< "Warning: the city is too big to draw on a map!"<< endl;
    return false;
  }
}


bool CityClass::getIsReadyForUse() const
{                                       //get the state to see if any
  return fileState;                     //file is loaded correctly
}

